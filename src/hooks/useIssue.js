import { h } from 'vue';

import JeIssue from '@/views/issues/dialog.vue';

import { Modal } from '@jecloud/ui';

export function showIssueWin({ id }, fn) {
  let selfModel = {};
  selfModel = Modal.window({
    title: '问题反馈',
    resize: false,
    width: '1200px',
    maximizable: false,
    content() {
      return h(JeIssue, { id, selfModel });
    },
    //关闭方法
    // eslint-disable-next-line no-unused-vars
    onClose(model) {
      fn();
    },
  });
}
