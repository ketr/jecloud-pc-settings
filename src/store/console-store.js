import { defineStore } from 'pinia';

export const useConsoleStore = defineStore({
  id: 'console-store',
  state: () => ({
    checkTreeRow: {}, // 选中行
    treeData: [], // 树数据
    activeTabKey: 'tablehome', // 激活的key
    tabPaneList: [
      {
        title: '首页',
        content: '欢迎来到资源表的世界！',
        key: 'tablehome',
        closable: false,
        icon: 'icon-home',
      },
    ], // 标签页
    elHeight: 0, // 高度
    treeRef: null, // 树
  }),
  getters: {
    getTabPaneList: (state) => state.tabPaneList,
    getActiveKey: (state) => state.activeTabKey,
    getConsoleTreeList: (state) => state.treeData,
    getCheckTreeRow: (state) => state.checkTreeRow,
    getElHeight: (state) => state.elHeight,
  },
  actions: {
    setElHeight(treeRef) {
      this.treeRef = treeRef;
      this.elHeight = treeRef?.$el?.offsetHeight;
    },

    //选中的数据
    checkedRow(row, newValue) {
      this.checkTreeRow = row || {};
      if (newValue != '' && newValue != undefined && newValue != null) {
        const { code, text, id } = row;
        this.setTabPanelList({
          title: text,
          key: `${code}`,
          closable: true,
          id,
        });
        // 说明是选中
        this.setTabPanelListActiveKey(code);
      }
    },
    // 激活的key
    setTabPanelListActiveKey(key) {
      this.activeTabKey = key;
    },
    setTabPanelList(item) {
      if (this.tabPaneList.some((item_) => item_.key == item.key)) {
        return;
      }
      this.tabPaneList.push(item);
    },
    // tree的数据
    setTreeData(data) {
      this.treeData = data;
    },
    checkedTreeRow(id) {
      const row = this.treeRef.getRowById(id);
      this.treeRef.setTreeExpand4Path({ row, expand: true }).then((row) => {
        this.treeRef.setSelectRow(row);
        this.treeRef.scrollToRow(row);
      });
    },
    removeTabItem(key) {
      const index = this.tabPaneList.findIndex((item) => item.key == key);
      if (index > -1) {
        this.tabPaneList.splice(index, 1);
        this.activeKey = this.tabPaneList[index - 1].key;
        if (this.activeKey == 'tablehome') {
          this.checkedTreeRow(null);
        } else {
          this.checkedTreeRow(this.tabPaneList[index - 1].id);
        }

        this.setTabPanelListActiveKey(this.activeKey);
      }
    },
  },
});
