import { decode } from '@jecloud/utils';
//#region 将全局数据转换成页面数据
export const changeObj = (Array) => {
  let arr = [];
  let obj = {};
  function recurve(params) {
    params.map((item) => {
      obj = {};
      obj['code'] = item.code;
      obj['items'] = item.items;
      obj['type'] = item.type;
      arr.push(obj);
      if (item.childSettings) {
        recurve(item.childSettings);
      }
    });
  }
  recurve(Array);
  return arr;
};
//#endregion
//#region 合并两张表（可能要废弃）
export const mergeArray = (arr1, arr2) => {
  arr1.map((item) => {
    arr2.find((td) => {
      if (td.code === item.code) {
        //文档设置特殊处理
        if (td.code == 'document-setting') {
          const setArr = decode(td.items[0].value) || [];
          return item.items.map((i) => {
            setArr.find((e) => {
              if (i.type === e.type) {
                i.type = e.type;
                i.format = e.format;
                i.previewWay = e.previewWay;
              }
            });
          });
        } else {
          return item.items.map((i) => {
            td.items.find((e) => {
              if (i.code === e.code) {
                i.value = e.value;
                // i[e.code] = e.code;
                i.type = e.type;
              }
            });
          });
        }
      }
    });
  });
  return arr1;
};
//#endregion
// #region 滚动及淡入动画
export const onScroll = () => {
  let classKey = '';
  const parentNode =
    document.getElementsByClassName('platform-develop')[0] ||
    document.getElementsByClassName('platform-manage')[0];
  const node = parentNode.querySelector('.je-panel-item-region-default');
  classKey = localStorage.getItem('classKey') || '';
  localStorage.setItem('classKey', '');
  if (classKey) {
    const myClass = document.getElementsByClassName(classKey)[0];
    if (myClass) {
      myClass?.classList.add('fade-animation');
      setTimeout(() => {
        myClass?.classList.remove('fade-animation');
      }, 3000);
      const clientHeight = document.body.clientHeight;
      const { top, height } = myClass?.getBoundingClientRect();
      if (clientHeight < top + height) {
        node.scrollTop = top + 2 * height - clientHeight;
      } else if (top - height <= 0) {
        node.scrollTop = -2 * height - top;
      }
    }
  }
};
// #endregion
// #region 保证进入top为卷曲
export const scrollToTop = () => {
  const parentNode =
    document.getElementsByClassName('platform-develop')[0] ||
    document.getElementsByClassName('platform-manage')[0];
  const node = parentNode.querySelector('.je-panel-item-region-default');
  node.scrollTop = 0;
};
// #endregion
