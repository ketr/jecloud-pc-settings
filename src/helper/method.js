// 对比两数组是否一致
export const isArrEqual = (arr1, arr2) => {
  const res1 = arr1.every((x) => {
    return arr2.includes(x);
  });
  const res2 = arr2.every((x) => {
    return arr1.includes(x);
  });
  return res1 && res2;
};
