// 用于系统设置回显
const data = [
  {
    code: 'mail-config',
    items: [
      { code: 'JE_SYS_EMAIL_SENDTITLE', value: '', type: '' },
      { code: 'JE_SYS_EMAIL_SSL', value: '', type: '' },
      { code: 'JE_SYS_EMAIL_SERVERPORT', value: '', type: '' },
      { code: 'JE_SYS_EMAIL_AUTHCODE', value: '', type: '' },
      { code: 'JE_SYS_EMAIL_SERVERVALIDATE', value: '', type: '' },
      { code: 'JE_SYS_EMAIL_SERVERHOST', value: '', type: '' },
      { code: 'JE_SYS_EMAIL_PASSWORD', value: '', type: '' },
      { code: 'JE_SYS_EMAIL_USERNAME', value: '', type: '' },
    ],
  },
  {
    code: 'password-config',
    items: [
      { code: 'JE_CORE_ERRORPW', value: '', type: '' },
      { code: 'JE_REGEXP_ENABLE', value: '', type: '' },
      { code: 'REGEXP', value: '', type: '' },
      { code: 'REGEXP_NUMBER', value: '', type: '' },
      { code: 'JE_SYS_PASSWORD', value: '', type: '' },
      { code: 'INITPASSWORD', value: '', type: '' },
    ],
  },
  {
    code: 'open-config',
    items: [
      { code: 'JE_OPEN_DD', value: '', type: '' },
      { code: 'JE_OPEN_FS', value: '', type: '' },
      { code: 'JE_OPEN_WX', value: '', type: '' },
      { code: 'JE_OPEN_WELINK', value: '', type: '' },
    ],
  },
  {
    code: 'open-welink-config',
    items: [
      { code: 'OPEN_WL_CLIENTID', value: '', type: '' },
      { code: 'OPEN_WL_CLIENTSECRET', value: '', type: '' },
    ],
  },
  {
    code: 'open-dingding-config',
    items: [
      { code: 'OPEN_DD_APPSECRET', value: '', type: '' },
      { code: 'OPEN_DD_CORPID', value: '', type: '' },
      { code: 'OPEN_DD_AGENTID', value: '', type: '' },
      { code: 'OPEN_DD_APPKEY', value: '', type: '' },
    ],
  },
  {
    code: 'open-feishu-config',
    items: [
      { code: 'OPEN_FS_APPID', value: '', type: '' },
      { code: 'OPEN_FS_APPSECRET', value: '', type: '' },
    ],
  },
  {
    code: 'open-wechat-config',
    items: [
      { code: 'OPEN_WX_CORPID', value: '', type: '' },
      { code: 'OPEN_WX_CORPSECRET', value: '', type: '' },
      { code: 'OPEN_WX_SECRET', value: '', type: '' },
      { code: 'OPEN_WX_AGENTID', value: '', type: '' },
    ],
  },
  { code: 'manage-basic', items: [] },
  {
    code: 'manage-plan',
    items: [
      { code: 'JE_SYS_PATHSCHEMENAME', value: '', type: '' },
      { code: 'JE_SYS_PATHSCHEME', value: '', type: '' },
      //{ code: 'JE_SYS_AVAILABLEPRODUCTS', value: '', type: '' },
      { code: 'SETPLAN_ASSOCIATE_TOP_MENU_ID', value: '', type: '' },
      { code: 'SETPLAN_IS_DEFULT', value: '', type: '' },
      { code: 'SETPLAN_YESORNOUSED', value: '', type: '' },
      { code: 'SETPLAN_ASSOCIATE_ALL_TOP_MENU', value: '', type: '' },
      { code: 'JE_SYS_LOGINMETHOD', value: '', type: '' },
      { code: 'JE_SYS_LOGINCOLOR', value: '', type: '' },
      { code: 'JE_SYS_TITLELOGO', value: '', type: '' },
      { code: 'JE_SYS_BACKGROUNDLOGO', value: '', type: '' },
      { code: 'JE_SYS_LEFTIMG', value: '', type: '' },
      { code: 'JE_SYS_SYSNAME', value: '', type: '' },
      { code: 'JE_SYS_SYSURL', value: '', type: '' },
      { code: 'JE_SYS_ICON', value: '', type: '' },
      { code: 'JE_SYS_WEBTITLE', value: '', type: '' },
      { code: 'JE_SYS_WEBLOGO', value: '', type: '' },
      { code: 'JE_SYS_DEFAULT_HUE', value: '', type: '' },
      { code: 'JE_SYS_TOPMENU', value: '', type: '' },
      { code: 'JE_SYS_DEFAULT_MENUCOLOR', value: '', type: '' },
    ],
  },
  {
    code: 'netdisc-config',
    items: [
      {
        code: 'JE_SYS_DOCADMIN',
        value: '',
        type: '',
      },
      {
        code: 'JE_SYS_DOCADMIN_NAME',
        value: '',
        type: '',
      },
    ],
  },
  {
    code: 'company-config',
    items: [
      { code: 'SETPLAN_PRIVATE_ENABLE', value: '', type: '' },
      { code: 'JE_SYS_PATHSCHEMENAME', value: '', type: '' },
      { code: 'JE_SYS_PATHSCHEME', value: '', type: '' },
      { code: 'SETPLAN_IS_DEFULT', value: '', type: '' },
      { code: 'SETPLAN_YESORNOUSED', value: '', type: '' },
      { code: 'SETPLAN_ASSOCIATE_ALL_TOP_MENU', value: '', type: '' },
      { code: 'JE_SYS_LOGINMETHOD', value: '', type: '' },
      { code: 'JE_SYS_LOGINCOLOR', value: '', type: '' },
      { code: 'JE_SYS_TITLELOGO', value: '', type: '' },
      { code: 'JE_SYS_BACKGROUNDLOGO', value: '', type: '' },
      { code: 'JE_SYS_LEFTIMG', value: '', type: '' },
      { code: 'JE_SYS_SYSNAME', value: '', type: '' },
      { code: 'JE_SYS_SYSURL', value: '', type: '' },
      { code: 'JE_SYS_ICON', value: '', type: '' },
      { code: 'JE_SYS_WEBTITLE', value: '', type: '' },
      { code: 'JE_SYS_WEBLOGO', value: '', type: '' },
      { code: 'JE_SYS_DEFAULT_HUE', value: '', type: '' },
      { code: 'JE_SYS_TOPMENU', value: '', type: '' },
      { code: 'JE_SYS_DEFAULT_MENUCOLOR', value: '', type: '' },
    ],
  },
];

export default data;
